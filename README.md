# *Custom* Localhost
![alt](assets/images/all-logo.png)

*Your eyes deserve better than localhost default style!*

[![PHP 7.2](https://img.shields.io/badge/PHP-7.2-blue)](https://www.php.net)
[![Sass 1.23](https://img.shields.io/badge/Sass-1.23-ff69b4)](https://sass-lang.com)
[![NodeJS 11.15](https://img.shields.io/badge/NodeJS-11.15-green)](https://nodejs.org/en)
[![Yarn 1.19](https://img.shields.io/badge/Yarn-1.19-blue)](https://nodejs.org/en)  

**TODO**  
- get readme content
- get dep infos and display it with badges

---

1. [**Concept**](#1-concept)
    * a. [*What is necessary ?*](#a-what-is-necessary)
2. [**Installation**](#2-installation)

---

## **1. Concept**
*Localhost display just list of directories in `html` directory, it's functional but not pretty. Custom localhost is useful to add better style on your localhost. It's display PHP Info and phpMyAdmin and list of respositories but with some features like git url, author or readme link.*  

![alt](assets/images/preview.png)

### ***a. What is necessary ?***
You just need to have a localhost like Apache or Nginx with LAMP/LEMP. And activate `index.php` on options.

---

## 2. Installation
`git clone` the repository, or download it and unzip it, to localhost folder :
- Linux (default) : `/var/www/html/`

Move `index.php` to root of localhost folder and you will see the new design for localhost.

You will have file structure like this:

```
var/www/html/
│   index.php
|   ... 
│
└───localhost-custom/
│   │   assets/
│   │   index-app.php
```