<?php
    header("Content-type: text/css; charset: UTF-8");
    include '../php/variables.php';
?>

.parallax {
    /* The image used */
    background-image:
        linear-gradient(rgba(255, 255, 255, 0.75),
            rgba(255, 255, 255, 0.95)),
        url('../images/<?php echo $server; ?>-wallpaper.png');

    /* Full height */
    height: 100%;

    /* Create the parallax scrolling effect */
    background-attachment: fixed;
    background-position: center;
    background-repeat: no-repeat;
    background-size: 50%;
}