document.addEventListener('DOMContentLoaded', function() {
    var sidenav = document.querySelectorAll('.sidenav');
    var instanceSidenav = M.Sidenav.init(sidenav);
    var tooltip = document.querySelectorAll('.tooltipped');
    var instanceTooltip = M.Tooltip.init(tooltip);
});

var repositories = document.getElementById('repositories');
var php_info = document.getElementById('php-info');

var php_info_child = php_info.childNodes;
php_info_child[1].remove();

function phpInfoSwitch(navbar) {
    navbar.parentNode.classList.toggle('active');
    repositories.classList.toggle('display-none');
    php_info.classList.toggle('display-none');
}

function refreshPage() {
    document.location.reload(true);
}

for (let index = 0; index < repositoriesToJs.length; index++) {
    const element = repositoriesToJs[index];
    var openGraphImage = document.querySelectorAll('.git-image-og')
    openGraphImage[index].setAttribute("src", getOpenGraph(element.url));
}

function getOpenGraph(og_url) {
    let urlEncoded = encodeURIComponent(og_url);
    let apiKey = '50dbad17-ed08-47b0-91ad-0e8178a2ada0';
    let requestUrl = 'https://opengraph.io/api/1.1/site/' + urlEncoded + '?app_id=' + apiKey;
    var json_obj = JSON.parse(Get(requestUrl));
    if (json_obj.error != undefined) {
        if (json_obj.error.message == 'Rate limit exceeded') {
            console.error('Rate limit exceeded for opengraph.io API')
            return 'assets/images/git-logo.png'
        }
        console.error(json_obj.error)
    }
    else {
        return json_obj.hybridGraph.image;
    }
}

function Get(yourUrl){
    var Httpreq = new XMLHttpRequest(); // a new request
    Httpreq.open("GET",yourUrl,false);
    Httpreq.send(null);
    return Httpreq.responseText;          
}