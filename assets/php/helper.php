<?php

function getGitRepoLink($git_repo_name) {
    if (file_exists($git_repo_name.'/.git/config')) {
        $git_config_raw = file_get_contents($git_repo_name.'/.git/config');
        $git_repo_status = 
            preg_match_all(
                '#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#',
                $git_config_raw,
                $git_link
            );
        $git_link = $git_link[0][0];
        if ($git_repo_status == 0) {
            $git_ssh = explode("\n", file_get_contents($git_repo_name.'/.git/config'));
            $git_ssh_link = [];
            foreach($git_ssh as $item) {
                $data = explode("url", $item);
                $data = str_replace(' = git@','',$data[1]);
                $git_ssh_link[] = $data;
            }
            $git_ssh_link = array_filter($git_ssh_link);
            $git_ssh_link = $git_ssh_link[6];
            $git_ssh_link = str_replace('.git','',$git_ssh_link);
            $git_ssh_link = explode(":", $git_ssh_link);
            $git_ssh_link = implode('/',$git_ssh_link);
            $git_ssh_link = 'https://'.$git_ssh_link;

            if (strpos($git_ssh_link, '=') !== false) {
                $git_ssh_link = str_replace(' = forge@','',$git_ssh_link);
                return $git_ssh_link;
            }

            return $git_ssh_link;
        }
        return $git_link;
    }

    return false;
}

function getGitRepoClone($git_repo_name) {
    if (file_exists($git_repo_name.'/.git/config')) {
        $git_config_raw = file_get_contents($git_repo_name.'/.git/config');
        $git_repo_status = 
            preg_match_all(
                '#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#',
                $git_config_raw,
                $git_link
            );
        if ($git_repo_status == 0) {
            return 'SSH';
        }
        return 'HTTPS';
    }
    return false;
}

function getGitAuthor($git_repo_link) {
    $author = explode('/',preg_replace('#^https?://#', '', $git_repo_link));
    $author = $author[1];
    return $author;
}

function getGitAuthorProfile($git_repo_link) {
    $author_profile_link = explode('/',preg_replace('#^https?://#', '', $git_repo_link));
    $author_profile_link = 'https://'.$author_profile_link[0].'/'.$author_profile_link[1];
    return $author_profile_link;
}

function getGitDomain($git_repo_link,$git_domains) {
    $git_domain = explode('/',preg_replace('#^https?://#', '', $git_repo_link));
    $git_domain = str_replace('.com','',$git_domain[0]);
    foreach ($git_domains as $key => $value) {
        if ($git_domain == $key) {
            return $value;
        }
    }
    return '<i class="material-icons left"><i class="fab fa-git-alt"></i></i> Git';
}

?>