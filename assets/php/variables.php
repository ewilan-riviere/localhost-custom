<?php

$dir_localhost    = '/var/www/html/';

$server = $_SERVER['SERVER_SOFTWARE'];

$php_version = substr(phpversion(),0,5);
$apache_version = 'not found';
if(strpos($server, 'Apache') !== false) {
    $apache_version = substr(apache_get_version(),7,6);
}

$nginxVersion = shell_exec('nginx -v 2>&1');
$mysqlVersion = shell_exec('mysql --version');

if (strpos($server, 'nginx') !== false) {
    $server = 'nginx';
    $server_name = 'NGINX';
} else if(strpos($server, 'Apache') !== false) {
    $server = 'apache';
    $server_name = 'Apache';
} else {
    $server = 'unknown';
    $server_name = 'Unknown';
}

$dropdown = [ "Documentation", "Git" , "Serveurs" ];

$navbar_arr = array(
    'Navbar' => array(
        'phpmyadmin' => '⛵ phpMyAdmin',
    ),
    'Dropdown' => array(
        'Git_icon' => '<i class="fab fa-git-alt"></i>',
        'Documentation_icon' => '<i class="fas fa-book"></i>',
        'Serveurs_icon' => '<i class="fas fa-server"></i>',
        'Git' => array(
            'https://github.com/' => '<i class="fab fa-github"></i> GitHub',
            'https://gitlab.com/' => '<i class="fab fa-gitlab"></i> GitLab',
            'https://framagit.org/' => '<i class="fab fa-gitlab"></i> Framagit',
            'https://bitbucket.org/' => '<i class="fab fa-bitbucket"></i> BitBucket',
        ),
        'Documentation' => array(
            'https://laravel.com/docs/5.8' => '<i class="fab fa-laravel"></i> Laravel',
            'https://vuejs.org/v2/guide/' => '<i class="fab fa-vuejs"></i> VueJS',
            'https://getbootstrap.com/docs/4.3/getting-started/introduction/' => '<i class="fab fa-bootstrap"></i> Bootstrap',
            'https://www.w3schools.com/' => '<i class="fas fa-bookmark"></i> W3Schools',
            'https://fontawesome.com/icons?d=gallery' => '<i class="fab fa-font-awesome-flag"></i> Font Awesome',
            'https://github.com/ewilan-riviere/localhost-custom' => '<i class="fab fa-github"></i> Localhost Custom',
        ),
        'Serveurs' => array(
            'https://cpanel.hostinger.fr/hosting/index/aid/31814154' => '<i class="fas fa-server"></i> Hostinger',
            'https://ewilan-riviere.tech/' => '<i class="fas fa-globe"></i> Ewilan Rivière',
            'http://dev.ewilan-riviere.tech/' => '<i class="fas fa-laptop-code"></i> Ewilan Rivière (dev)',
            'http://127.0.0.1:8000/' => '<i class="fab fa-laravel"></i> Laravel Server',
        )
    ),
);

// list of all git
$git_multi_array = [
    'github' => '<i class="fab fa-github"></i> GitHub',
    'gitlab' => '<i class="fab fa-gitlab"></i> GitLab',
    'framagit' => '<i class="fab fa-gitlab"></i> Framagit',
    'bitbucket' => '<i class="fab fa-bitbucket"></i> BitBucket'
];

$navbar_form = array(
    [
        'name' => 'Font Awesome',
        'link' => 'https://fontawesome.com/icons',
        'logo' => '<i class="fab fa-font-awesome-flag"></i>'
    ],
    [
        'name' => 'Google',
        'link' => 'https://www.google.com/search',
        'logo' => '<i class="fab fa-google"></i>'
    ]
);

function sshGit($git_ssh) {
    $data = "a";
    foreach($git_ssh as $item) {
        $data = explode("url", $item);
        $data = str_replace('= git@','',$data[1]);
        return $data;
    }
    return $data;
}

function searchDepVers($json,$replace)
{
    $dep_v = str_replace('^','',str_replace('"','',str_replace($replace.'": ','',substr($json,strpos($json, $replace),30))));
    $dep_v = preg_replace( '/[^0-9.]/', '', $dep_v );
    if (substr($dep_v,-1) == '.') {
        $dep_v = rtrim($dep_v,".");
    }
    return $dep_v;
}

function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}

?>