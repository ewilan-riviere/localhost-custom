<?php
    $navbar_array = [
        $phpmyadmin = 
            New NavbarItem(
                'phpMyAdmin',
                '/phpmyadmin',
                '<i class="fas fa-database"></i>',
                false
            )
        // $gitlab = 
        //     New NavbarItem(
        //         'GitLab',
        //         'https://gitlab.com/',
        //         '<i class="fab fa-gitlab"></i>',
        //         true
        //     ),
    ];
?>
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper <?php printf($server) ?>-color">
            <a class="brand-logo pointer" onclick="refreshPage()">
                <div class="logo-container">
                    <div class="logo-effect">
                        <img src="<?php echo $dir; ?>assets/images/<?php echo $server; ?>-logo.png" alt="" class="logo-image">
                        <?php
                            echo $server_name;
                        ?>
                    </div>
                </div>
            </a>
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <a class="waves-effect" onclick="phpInfoSwitch(this)">
                        <i class="material-icons left"><i class="fab fa-php"></i></i> 
                        <span class="font-weight-bold">PHP Info</span>
                    </a>
                </li>
                <?php
                    foreach ($navbar_array as $nav_item) {
                        echo '<li>';
                            echo '<a class="waves-effect" href="'.$nav_item->url.'"';
                                if ($nav_item->status) {
                                    echo 'target="_blank"';
                                }
                            echo '>';
                                echo '<i class="material-icons left">'.$nav_item->logo.'</i>';
                                echo '<span class="font-weight-bold">'.$nav_item->name.'</span>';
                            echo '</a>';
                        echo '</li>';
                    }
                ?>
            </ul>
        </div>
    </nav>
</div>

<ul id="slide-out" class="sidenav">
    <li>
        <div class="user-view">
            <div class="background">
                <!-- <img src="<?php echo $dir; ?>assets/images/<?php echo $server; ?>-logo.png"> -->
            </div>
            <a href="#user">
                <img class="circle" src="<?php echo $dir; ?>assets/images/<?php echo $server; ?>-logo.png">
            </a>
            <a href="#name">
                <span class="black-text name">
                    <?php
                        echo $server_name;
                    ?>
                </span>
            </a>
            <a href="#email">
            <span class="white-text email">jdandturk@gmail.com</span>
            </a>
        </div>
    </li>
    <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
    <li><a href="#!">Second Link</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Subheader</a></li>
    <?php
        foreach ($navbar_array as $nav_item) {
            echo '<li>';
                echo '<a class="sidenav-close waves-effect" href="'.$nav_item->url.'"';
                    if ($nav_item->status) {
                        echo 'target="_blank"';
                    }
                echo '>';
                    echo '<i class="material-icons">'.$nav_item->logo.'</i> ';
                    echo $nav_item->name;
                echo '</a>';
            echo '</li>';
        }
    ?>
</ul>