<?php
$dir_localhost = new DirectoryIterator($dir_localhost);

$i = 0;
$repositories = [];
$git_domains = [
    'github' => '<i class="material-icons left"><i class="fab fa-github"></i></i> GitHub',
    'gitlab' => '<i class="material-icons left"><i class="fab fa-gitlab"></i></i> GitLab',
    'framagit' => '<i class="material-icons left"><i class="fab fa-git"></i></i> Framagit',
    'bitbucket' => '<i class="material-icons left"><i class="fab fa-bitbucket"></i></i> BitBucket'
];

foreach ($dir_localhost as $file) {

    if (!$file->isDot()
        && $file->isDir()
        && $file->getFilename() != 'localhost-custom'
        && $file->getFilename() != 'phpmyadmin') {
        
        $git_repo_name = $file->getFilename();

        $title = ucwords(str_replace("-"," ",$git_repo_name));
        $git_repo_link = getGitRepoLink($git_repo_name);
        $git_clone = getGitRepoClone($git_repo_name);
        $git_domain = getGitDomain($git_repo_link,$git_domains);
        $logo = '';
        $git_readme_link = $git_repo_link.'/blob/master/README.md';
        $readme = '';
        $git_author = getGitAuthor($git_repo_link);
        $git_author_profile = getGitAuthorProfile($git_repo_link);

        $fileName = 
            new Repository(
                $git_repo_name,
                $title,
                $git_repo_link,
                $git_clone,
                $git_domain,
                $logo,
                $git_readme_link,
                $readme,
                $git_author,
                $git_author_profile,
                true
            );
        // $fileName = $file->getFilename();
        /** 
         *  Place into an Array
         **/
        $repositories[] = $fileName;
    }
}

usort($repositories, function($a, $b)
{
    return strcmp($a->title, $b->title);
});

// echo '<pre>';
// echo var_dump($repositories);
// echo '</pre>';

foreach ($repositories as $repository) {
?>
    <div class="col s12 m6 l4 xl3">
        <div class="card">
            <div class="card-image">
                <div class="git-image-og-container">
                    <img src="" alt="git-logo" id="gitImageOg<?php echo $i ?>" class="git-image-og" />
                </div>
                <!-- <span class="card-title">Card Title</span> -->
            </div>
            <div class="card-content">
                <span class="card-title">
                    <?php
                        echo $repository->title;
                    ?>
                </span>
                <div class="mb-3">
                    de 
                    <a
                        href="
                        <?php
                            echo $repository->author_profile
                        ?>"
                        target="_blank"
                    >
                        <?php
                            echo $repository->author
                        ?>
                    </a>
                </div>
                <div>
                    <div>
                        Dépôt : 
                        <?php
                            echo '<code>'.$repository->repository_name.'</code> (en '.$repository->git_clone.')'
                        ?>
                    </div>
                </div>
            </div>
            <div class="card-action">
                <a
                    id="git_link_<?php echo $i ?>"
                    href="<?php echo $repository->url ?>"
                    target="_blank"
                    class="btn git_link_color font-weight-bold my-auto git_btn mb-2 tooltipped"
                    data-position="bottom"
                    data-tooltip="Afficher le dépôt"
                >
                    <?php
                        echo $repository->git_domain
                    ?>
                </a>
                <a
                    href="
                    <?php
                        $repo_launch = $repository->repository_name;
                        if (file_exists($repository->repository_name.'/composer.json')) {
                            $repo_launch = $repo_launch.'/public';
                        }
                        echo $repo_launch;
                    ?>"
                    class="btn font-weight-bold tooltipped"
                    data-position="top"
                    data-tooltip="Afficher l'index"
                >
                    <i class="material-icons">launch</i>
                </a>
                <a
                    href="
                    <?php
                        echo $repository->readme_link
                    ?>"
                    target="_blank"
                    class="btn font-weight-bold tooltipped"
                    data-position="bottom"
                    data-tooltip="Afficher le readme"
                >
                    <i class="material-icons">format_align_left</i>
                </a>
            </div>
        </div>
    </div>
<?php
$i++;
}
?>

<script>
    var repositoriesToJs = <?php echo json_encode($repositories); ?>;
</script>