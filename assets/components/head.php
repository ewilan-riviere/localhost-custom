<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>
    <?php
        echo $server_name.' server';
    ?>
</title>
<link
    rel="shortcut icon"
    href="localhost-custom/assets/images/<?php echo $server; ?>-logo.png"
>
<link
    rel="stylesheet"
    href="<?php echo $dir; ?>assets/css/app.css"
>
<link
    rel="stylesheet"
    href="<?php echo $dir; ?>assets/css/utilitary.css"
>
<link
    rel="stylesheet"
    href="<?php echo $dir; ?>assets/css/style.css"
>
<link
    rel="stylesheet"
    href="<?php echo $dir; ?>assets/css/php-info.css"
>
<link
    rel="stylesheet"
    href="<?php echo $dir; ?>assets/css/parallax.php"
>
<script
    src="<?php echo $dir; ?>assets/js/app.js"
></script>