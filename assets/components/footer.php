<footer class="page-footer <?php printf($server) ?>-color">
    <!-- <div class="container">
        <div class="row">
        <div class="col l6 s12">
            <h5 class="white-text">Footer Content</h5>
            <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
        </div>
        <div class="col l4 offset-l2 s12">
            <h5 class="white-text">Links</h5>
            <ul>
            <li><a class="grey-text text-lighten-3" href="#!">Link 1</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 2</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 3</a></li>
            <li><a class="grey-text text-lighten-3" href="#!">Link 4</a></li>
            </ul>
        </div>
        </div>
    </div> -->
    <div class="footer-copyright d-flex justify-content-between">
        <div class="my-auto ml-4">
            <?php
                echo 'PHP Version: '.$php_version;
            ?>
            <?php
                if ($server == 'apache') {
                    echo '&nbsp;/&nbsp;Apache Version: '.$apache_version;
                }
            ?>
        </div>
        <a class="footer-repo grey-text text-lighten-4 right d-flex mr-4" href="https://github.com/ewilan-riviere/localhost-custom" target="_blank">
            <i class="material-icons left"><i class="fab fa-github"></i></i>
            <span class="my-auto">Localhost Custom</span>
        </a>
    </div>
</footer>