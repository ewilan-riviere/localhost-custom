<?php
    // Variables
    include 'localhost-custom/php/variables.php';

    // Index
    include 'localhost-custom/php/components/head.php';
    
    include 'localhost-custom/php/components/navbar.php';
    include 'localhost-custom/php/components/body.php';
    
    include 'localhost-custom/php/components/foot.php';
?>