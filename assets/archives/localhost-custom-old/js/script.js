// Presentation of localhost
$('body').removeClass('parallax');
// $('main').addClass('full-height');
setTimeout(function () {
    $('#welcome').html('<div><h1>Bienvenue sur</h1><img src="localhost-custom/images/' + server + '-title.png" alt="' + server + '" /></div>');
}, 100);
setTimeout(function () {
    $('#welcome').fadeOut(400)
    setTimeout(function () {
        $('main').removeClass('full-height');
        $('#list-folder').fadeIn('slow');
        $('body').addClass('parallax');
    }, 500);
}, 500);

if ($('#git_link').is(':empty')) {
    $('#git_link').html('<i class="fab fa-git-alt"></i> Git')
}

var readme_arr = [];
for (let i = 0; i < readmejs.length; i++) {
    readme_arr.push(readmejs[i])

}

var readme_json = JSON.stringify(readme_arr)

function readme(id) {
    var readmeId = id[id.length - 1];
    // Creating a cookie after the document is ready
    document.getElementById('readme-display').innerHTML =
        marked(readmejs[readmeId]);

    $('#list-folder').fadeOut('fast');
    $('#readme-display').fadeIn('slow');
    $('#readme-return').fadeIn('slow');

    // $('#readme-display').html(readmejs[readmeId]);

}

document.querySelector('.search_input').addEventListener("submit", function(e){
    var x = document.getElementsByClassName('search_input');
    var i;
    for (i = 0; i < x.length; i++) {
        x[i].submit();
        x[i].value = "";
    }
});

function hideReadme() {
    $('#readme-display').fadeOut('fast');
    $('#readme-return').fadeOut('fast');
    deleteCookie("readmejs");
    $('#list-folder').fadeIn('slow');
}

function selectRepository(id) {
    $('#card-display-preview-attr').hide();
    $('#card-display-no-preview').hide();
    $('#card-display-git-url').show();
    $('#readme-card').hide();
    $('#card-display-readme-switch').show();
    $('#card-display-readme').hide();

    $('#card-display-readme-switch').addClass('btn-outline-success text-success');
    $('#card-display-readme-switch').removeClass('btn-success text-white');

    id_arr = id.replace('card', '');
    badges = '';
    index = '';
    display = false;
    readme = false;
    author = false;
    og_url = repo_arr[id_arr]['git-url'];
    var urlEncoded = encodeURIComponent(og_url);
    var apiKey = '5f55aadf-63bf-4516-852d-e4ed9dc97c89'; // <-- Replace with your AppId

    // The entire request is just a simple get request with optional query parameters
    var requestUrl = 'https://opengraph.io/api/1.1/site/' + urlEncoded + '?app_id=' + apiKey;
    $('#card-display-description').text('');
    $('#card-display-logo').attr('src', '');
    $.getJSON(requestUrl, function (json) {
        // Throw the object in the console to see what it looks like!
        // console.log('json', json);

        // Update the HTML elements!
        // $('#title').text(json.hybridGraph.title);
        $('#card-display-description').text(json.hybridGraph.description);
        $('#card-display-logo').attr('src', json.hybridGraph.image);
        // window.history.pushState(url_og, "Info", "?url_og=" + url_og);
    });

    if (repo_arr[id_arr]['badges'] != null) {
        badges = repo_arr[id_arr]['badges'];
    }
    if (repo_arr[id_arr]['index'] != null) {
        index = repo_arr[id_arr]['index'];
    }

    if (index.includes('PHP Index')) {
        display = true;
    } else if (index.includes('HTML Index')) {
        display = true;
    }

    if (repo_arr[id_arr]['author']) {
        author = true;
    }

    $('#list-folder').hide();
    $('#card-display').fadeIn(1200);
    $('#card-display-title-title').html(repo_arr[id_arr]['title']);
    $('#card-display-repository').html('Dépôt : <code>' + repo_arr[id_arr]['repository'] + '</code>');
    $('#card-display-delete').attr('href', 'localhost-custom/php/library/unlink.php?file=' + repo_arr[id_arr]['repository']);
    if (author) {
        $('#card-display-author').text('de ' + repo_arr[id_arr]['author']);
    }
    if (display) {
        $('#card-display-preview-attr').show();
        $('#card-display-preview-attr').attr('href', repo_arr[id_arr]['repository']);
    } else {
        $('#card-display-no-preview').show();
        $('#card-display-no-preview').html('Ce dépôt ne dispose pas d\'un index et ne pas être prévisualisé ici.');
    }
    $('#card-display-badges').html(badges);
    git_repo = false;
    if (repo_arr[id_arr]['git-type'] != null) {
        git_repo = true;
        $('#card-display-git-type').html(repo_arr[id_arr]['git-type']);
    }
    if (git_repo) {
        $('#card-display-git-type').html(repo_arr[id_arr]['git-type']);
    } else {
        $('#card-display-git-type').html('Aucun .git');
    }

    if (repo_arr[id_arr]['git-url'] != null) {
        $('#card-display-git-url').addClass('btn btn-outline-success git_link_color font-weight-bold');
        $('#card-display-git-url').toggleClass('text-muted');
        $('#card-display-git-url').attr('href', repo_arr[id_arr]['git-url']);
    } else {
        $('#card-display-git-url').removeAttr('href');
        $('#card-display-git-url').addClass('text-muted');
        $('#card-display-git-url').removeClass('btn btn-outline-success git_link_color font-weight-bold');
    }

    if (repo_arr[id_arr]['readme']) {
        readme = true;
        $('#card-display-readme-switch').removeClass('text-muted');
        $('#card-display-readme-switch').addClass('btn font-weight-bold');
        $('#card-display-readme-switch').html('Readme');
    } else {
        $('#card-display-readme-switch').removeClass('btn font-weight-bold');
        $('#card-display-readme-switch').addClass('text-muted');
        $('#card-display-readme-switch').html("Aucun readme");
    }

    if (repo_arr[id_arr]['readme']) {
        $('#card-display-readme').html(marked(repo_arr[id_arr]['readme']));
    } else {

    }
}

function switchReadme() {
    $('#readme-card').fadeToggle();
    $('#card-display-readme').fadeToggle();
    $('#card-display-readme-switch').toggleClass('btn-success');
    $('#card-display-readme-switch').toggleClass('btn-outline-success');
    $('#card-display-readme-switch').toggleClass('text-success');
    $('#card-display-readme-switch').toggleClass('text-white');
}

function localhost() {
    $('#php-info').fadeOut('fast');
    $('#card-display').hide();
    $('#list-folder').fadeIn('slow');
}

function displayPHP() {
    $('#list-folder').fadeOut('fast');
    $('#php-info').fadeIn('slow');
}

// Function to create the cookie 
function createCookie(name, value, days) {
    var expires;

    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }

    document.cookie = escape(name) + "=" +
        escape(value) + expires + "; path=/";
}

function deleteCookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

for (let index = 0; index < readme_json.length; index++) {
    $('#cardId' + index).hover(function () {
        // $('#cardIdTitle'+index).css("font-weight","600");
    }, function () {
        // $('#cardIdTitle'+index).css("font-weight", '400');
    });
}