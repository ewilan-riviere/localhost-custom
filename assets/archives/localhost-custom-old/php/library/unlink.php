<?php

include '../variables.php';

$f = $dir.$_GET['file'];
chmod($f,0775);
chown($f,'www-data');

function rrmdir($dir) { 
    $delete_success = false;
    if (is_dir($dir)) { 
        $objects = scandir($dir); 
        foreach ($objects as $object) { 
            if ($object != "." && $object != "..") { 
                if (is_dir($dir."/".$object))
                rrmdir($dir."/".$object);
                else
                unlink($dir."/".$object); 
            } 
        }
        rmdir($dir);
        return $delete_success;
    }
    return $delete_success;
}
rrmdir($f);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Erreur d'écriture</title>
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="../../css/bootstrap/bootstrap.min.css">
</head>
<body class="m-2">
    <?php
        if (rrmdir($f)) {
            header('Location: http://localhost');
            die();
        } else {
            echo 'Impossible de supprimer le répertoire<br>';
            echo 'Droits d\'écriture '.substr(sprintf('%o', fileperms($f)), -4).'<br>';
            if (is_writable($f)) {
                echo 'Répertoire : is Writable<br>';
            } else {
                echo 'Répertoire : is not Writable<br>';
            }
            echo '<a href="/" class="btn btn-outline-danger">Retour</a>';
        }
    ?>
</body>
</html>