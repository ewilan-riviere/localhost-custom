<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?php echo $server_name; ?></title>
        <!-- Icon -->
        <link
            rel="shortcut icon"
            href="localhost-custom/images/<?php echo $server; ?>-logo.png"
        >
        <link rel="stylesheet" href="localhost-custom/css/bootstrap/bootstrap.min.css">
        <script src="localhost-custom/js/font-awesome/all.min.js"></script>
        <script src="localhost-custom/js/marked.js"></script>
        <link rel="stylesheet" href="localhost-custom/css/style.css">
        <link rel="stylesheet" href="localhost-custom/css/parallax.php">
    </head>
    <body class="parallax">