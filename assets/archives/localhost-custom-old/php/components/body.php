<main class="flex-center position-ref full-height">
    <div class="content pb-3">
        <div>
            <?php
                // include 'apache/php/notes.php';
                // require 'localhost-custom/php/library/OpenGraph.php';
            ?>
        </div>
        <!-- Readme appear here -->
        <div id="readme-display"></div>
        <!-- Card Display -->
        <?php
            include 'localhost-custom/php/components/body/selected-card.php';
        ?>
        <!-- Card Display End -->
        
        <div id="welcome"></div>
        
        <button id="readme-return" class="btn btn-success display_none" onclick="hideReadme()">Retour</button>
        <div id="list-folder" class="container container-width display_none">
            <div class="row d-flex justify-content-center">
                <?php
                    include 'localhost-custom/php/components/body/foreach.php';
                ?>
            </div>
        </div>
        <div id="php-info" class="display_none text-dark">
            <?php
                phpinfo();
            ?>
        </div>
    </div>
</main>