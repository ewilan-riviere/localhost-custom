<nav class="navbar navbar-expand-lg sticky-top shadow p-3 mb-5 bg-white rounded navbar-light bg-light">
    <a class="navbar-brand mouse_pointer" onclick="localhost();">
        <img
            src="localhost-custom/images/<?php echo $server; ?>-title.png"
            width=""
            height="30"
            class="d-inline-block align-top"
            alt=""
        >
    </a>
    <button
        class="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent"
        aria-expanded="false"
        aria-label="Toggle navigation"
    >
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <!-- Main Navbar -->
            <li class="nav-item font-weight-bold">
                <a class="nav-link mouse_pointer" onclick="displayPHP();">
                    <i class="fab fa-php"></i> PHP
                </a>
            </li>
            <?php
                foreach ($navbar_arr as $key => $value) {
                    if ($key == 'Navbar') {
                        foreach ($value as $k => $v) {
            ?>
                            <li class="nav-item font-weight-bold">
                                <a class="nav-link" target="_blank" href="<?php echo $k ?>">
                                    <?php echo $v ?>
                                </a>
                            </li>
            <?php
                        }
                    }
                }
            ?>
            <!-- dropdown Navbar -->
            <?php
            for ($i=0; $i < sizeof($dropdown); $i++) { 
                    foreach ($navbar_arr as $key => $value) {
                        foreach ($value as $k => $v) {
                            if ($k == $dropdown[$i].'_icon') {
                                $icon = $v;
                            }
                            if ($k == $dropdown[$i]) { 
                            ?>
                                <li class="nav-item font-weight-bold dropdown">
                                        <a class="nav-link dropdown-toggle"
                                            href="#" id="dropdown"
                                            role="button"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false" >
                                            <?php echo $icon.' '.$k ?>
                                        </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdown">
                                    <?php
                                    foreach ($v as $k2 => $v2) {
                                    ?>
                                        <a class="dropdown-item"
                                            target="_blank"
                                            href="<?php echo $k2 ?>">
                                            <?php echo $v2 ?>
                                        </a>
                                    <?php
                                    } ?>
                                    </div>
                                </li>
                        <?php 
                            }
                        }
                    }
                }
                ?>
        </ul>
        <?php
            foreach ($navbar_form as $key => $value) {
                foreach ($value as $k => $v) {
                    if ($k == 'name') {
                        $key_low = str_replace(' ','-',strtolower($v));
                        $key_title = $v;
                    }
                    if ($k == 'link') {
                        $link = $v;
                    }
                }
        ?>
            <form target="_blank" class="form-inline mt-1 mx-2" action="<?php echo $link ?>" methode="GET">
                <div class="input-group mb-3">
                    <input type="text" class="form-control search_input input_radius input_border" type="search" name="q" placeholder="<?php echo $key_title ?>" aria-label="Search">
                    <div class="input-group-append">
                        <button class="input-group-text input_radius_logo input_logo_border" type="submit" id="search-<?php echo $key_low ?>">
                            <?php if($k == 'logo') echo $v ?>
                        </button>
                    </div>
                </div>
            </form>
        <?php
            }
        ?>
    </div>
</nav>