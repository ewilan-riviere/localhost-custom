<?php
    // increment var i for readme id
    $i++;
    // check .git dir to find main repository if exist and display it
    $git_file = file_get_contents($FoundFiles[$j].'/.git/config');

    // extract git name with url of repository
    preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $git_file, $git_link);

    // If SSH remote 
    if ($git_link[0][0] == null) {
        $git_ssh = explode("\n", file_get_contents($FoundFiles[$j].'/.git/config'));
        $git_ssh_link = array();
        foreach($git_ssh as $item) {
            $data = explode("url", $item);
            $data = str_replace(' = git@','',$data[1]);
            $git_ssh_link[] = $data;
        }
        $git_ssh_link = array_filter($git_ssh_link);
        $git_ssh_link = $git_ssh_link[6];
        $git_ssh_link = str_replace('.git','',$git_ssh_link);
        $git_ssh_link = explode(":", $git_ssh_link);
        $git_ssh_link = implode('/',$git_ssh_link);
        $git_ssh_link = 'https://'.$git_ssh_link;
    }

    // find git repo author
    $author = explode('/',preg_replace('#^https?://#', '', $git_link[0][0]));
    $author = $author[1];
    if ($git_ssh_link != null) {
        $author = explode('/',preg_replace('#^https?://#', '', $git_ssh_link));
        $author = $author[1];
    }
    
    // check readme if exist
    $readmeFile = file_get_contents($FoundFiles[$j].'/README.md');
    // add readme to array
    $readme_arr[] = $readmeFile;

    $title = $FoundFiles[$j];
    $title = str_replace("-"," ",$title);
    $title = ucwords($title);

    // check composer.json if exist
    $composer = (file_get_contents($FoundFiles[$j].'/composer.json')) ? file_get_contents($FoundFiles[$j].'/composer.json') : false ;
    $package = file_get_contents($FoundFiles[$j].'/package.json');
    $index_php = file_get_contents($FoundFiles[$j].'/index.php');
    $index_html = file_get_contents($FoundFiles[$j].'/index.html');

    $search_version = array(
        'php' => 'PHP',
        'laravel' => 'Laravel',
        'bootstrap' => 'Bootstrap',
        'sass' => 'SASS',
        'vue' => 'VueJS',
    );
    
    $php_v = searchDepVers($composer,'php');
    $laravel_v = searchDepVers($composer,'laravel/framework');
    $bootstrap_v = searchDepVers($package,'bootstrap');
    $sass_v = searchDepVers($package,'"sass": "^');
    $vue_v = searchDepVers($package,'vue');
?>