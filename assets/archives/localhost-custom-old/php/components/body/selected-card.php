<div id="card-display" class="display_none">
    <!-- Main Card -->
    <div id="main-card" class="card" style="width: 50vw;">
        <a class="btn btn-success mouse_pointer mb-4 font-weight-bold text-white" onclick="localhost();">
            <i class="fas fa-arrow-left go_to_localhost fa-1x"></i> <?php echo $server_name; ?>
        </a>
        <div class="d-flex justify-content-center">
            <img id="card-display-logo" class="card-img-top git-logo" src="" alt="">
        </div>
        <div class="card-body">
            <h5 class="card-title">
                <h1 id="card-display-title" class="font-weight-bold">
                    <span id="card-display-title-title"></span>
                    <small id="card-display-author" class="font-italic"></small>
                </h1>
            </h5>
            <p class="card-text">
                <div id="card-display-repository"></div>
                
                <div id="card-display-badges" class="mx-auto git-desc my-2"></div>
                <div id="card-display-infos" class="d-flex justify-content-center">
                    <p id="card-display-description" class="d-flex justify-content-center git-desc mx-auto">
                    </p>
                </div>
                <div class="mt-4">
                    <a id="card-display-git-url"
                        target="_blank"
                        class="text-muted mr-2">
                        <span id="card-display-git-type">
                        </span>
                    </a>
                    <a id="card-display-readme-switch"
                        class=" mouse_pointer ml-2 readme_repo"
                        onclick="switchReadme();">
                        Readme
                    </a>
                    <a id="card-display-delete" class="btn btn-outline-danger delete_repo ml-4" href="">
                        Supprimer
                    </a>
                    <div id="card-display-preview" class="mt-4">
                        <a id="card-display-preview-attr"
                            href=""
                            class="btn btn-outline-success font-weight-bold git_link_color">
                            Afficher
                        </a>
                    </div>
                    <div id="card-display-no-preview" class="mt-4"></div>
                </div>
            </p>
        </div>
    </div>
    <!-- Readme Card -->
    <div id="readme-card" class="card mt-5" style="width: 50vw;">
        <div class="card-body">
            <p id="card-display-readme" class="card-text text-left p-4"></p>
        </div>
    </div>
</div>