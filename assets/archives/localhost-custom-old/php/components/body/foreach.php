<?php
$dir = new DirectoryIterator($dir);
// var to check repository for readme
$i = -1;
$readme_arr = array();
$repo_arr = array();
$FoundFiles = array();

foreach ($dir as $file) {

    if (!$file->isDot()
        && $file->isDir()
        && $file->getFilename() != 'localhost-custom'
        && $file->getFilename() != 'phpmyadmin') {

        $fileName = $file->getFilename();
        /** 
         *  Place into an Array
         **/
        $FoundFiles[] = $fileName;
    }
}
sort($FoundFiles, SORT_NATURAL | SORT_FLAG_CASE);
for ($j=0; $j < sizeof($FoundFiles); $j++) { 
    include 'localhost-custom/php/components/body/var-after-if.php';
?>
<div class="col-sm-auto mx-1 my-auto">
    <div class="card card_overflow card_style my-2" style="width: 23em;">
        <a id="card<?php echo $i ?>" class="d-block" onclick="selectRepository(this.id);">
            <div class="card-central">
                <div id="cardId<?php echo $i ?>" class="card-central">
                    <h5 id="cardIdTitle<?php echo $i ?>" class="card-title">
                        <div>
                            <?php echo $title ?>
                        </div>
                    </h5>
                    <h6 class="card-subtitle mb-2 text-muted">
                        <?php if($git_link[0][0] != null || $git_ssh_link != null) {
                                echo 'de '.$author;
                            } else {
                                echo '&nbsp;';
                            }
                        ?>
                        <div class="mt-3">
                            Dépôt : <?php echo $FoundFiles[$j] ?>
                        </div>
                    </h6>
                    <p class="card-text card_readme_height">
                        <?php
                            // Add badges if detected
                            foreach ($search_version as $kversion => $vversion) {
                                if (${$kversion.'_v'}) {
                                    $repo_arr[$i]['badges'][] = '<span class="badge badge-pill '.$kversion.'_color font-italic mx-1">#'.$vversion.' '.${$kversion.'_v'}.'</span>';
                                }
                            }
                            if ($package) {
                                $repo_arr[$i]['badges'][] = '<span class="badge badge-pill nodejs_color font-italic mx-1">#NodeJS</span>';
                            }
                            if ($index_php) {
                                $repo_arr[$i]['index'][] = 'PHP Index';
                            }
                            if ($index_html) {
                                $repo_arr[$i]['index'][] = 'HTML Index';
                            }
                        ?>
                    </p>
                </div>
                <a id="git_link"
                    <?php
                        if( $git_link[0][0] != null ) {
                            // Add href if git repo detected
                            echo 'href="'; ?><?php echo $git_link[0][0].'"';
                        }
                        else if($git_ssh_link != null) {
                            echo 'href="'; ?><?php echo $git_ssh_link.'"';
                        }
                    ?>
                    target="_blank"
                    class="<?php if( $git_link[0][0] != null || $git_ssh_link != null ) {}
                        echo 'btn btn-outline-success git_link_color font-weight-bold'; ?> my-auto git_btn mb-2"
                >
                    <?php
                        // check git extract and git multi array
                        foreach ($git_multi_array as $kgit => $vgit) {
                            $key_lower = strtolower($kgit);
                            if (strpos( $git_link[0][0], $key_lower)) {
                                $repo_arr[$i]['git-type'] = $vgit;
                                echo $vgit;
                            }
                            if (strpos( $git_ssh_link, $key_lower)) {
                                $repo_arr[$i]['git-type'] = $vgit;
                                echo $vgit;
                            }
                        }
                        if ($git_link[0][0] == null && $git_ssh_link == null) {
                            $repo_arr[$i]['git-type'] = null;
                            echo '<span class="text-muted">Aucun .git</span>';
                        }
                    ?>
                </a>
            </div>
        </a>
    </div>
</div>
<?php
    $repo_arr[$i]['title'] = $title;
    $repo_arr[$i]['repository'] = $FoundFiles[$j];
    $repo_arr[$i]['author'] = $author;
    if ($git_link[0][0] != null) {
        $repo_arr[$i]['git-url'] = $git_link[0][0];
    } else if($git_ssh_link != null) {
        $repo_arr[$i]['git-url'] = $git_ssh_link;
    } else {
        $repo_arr[$i]['git-url'] = '';
    }
    $repo_arr[$i]['readme'] = $readmeFile;
}
?>