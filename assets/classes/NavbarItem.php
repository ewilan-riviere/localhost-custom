<?php

class NavbarItem {
    var $name; 
    var $url;
    var $logo;
    var $status;

    public function __construct($name, $url, $logo, $status) 
    { 
        $this->name = $name;
        $this->url = $url;
        $this->logo = $logo;
        $this->status = $status;
    }
    
    function setName($name){ 
        $this->name = $name; 
    }
    function getName(){ 
        echo $this->name."<br>"; 
    }

    function setUrl($url){ 
        $this->url = $url; 
    } 
    function getUrl(){ 
        echo $this->url."<br>" ; 
    }
    
    function setLogo($logo){ 
        $this->logo = $logo; 
    }
    function getLogo(){ 
        echo $this->logo."<br>"; 
    }

    function setStatus($status){ 
        $this->status = $status; 
    }
    function getStatus(){ 
        echo $this->status."<br>"; 
    }
}