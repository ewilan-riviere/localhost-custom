<?php
    $dir='localhost-custom/';
    include 'assets/php/helper.php';
    // Classes
    include $dir.'assets/classes/NavbarItem.php';
    include $dir.'assets/classes/Repository.php';
    
    include 'assets/php/variables.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php
            include 'assets/components/head.php';
        ?>
    </head>
    <body class="parallax">
        <?php
            include 'assets/components/navbar.php';
            include 'assets/components/content.php';
            include 'assets/components/footer.php';
        ?>
    </body>
    <script src="<?php echo $dir; ?>assets/js/script.js"></script>
</html>